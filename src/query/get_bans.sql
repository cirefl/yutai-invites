with ancestors as
        (select root as user from parent where child = $user),
    addresses as
        (select address from address where user = $user),
    same_address as
        (select address, user from address natural join addresses),
    saa as
        (select user from ancestors natural join same_address group by (user))
select *
from ban
where (expires > unixepoch() or expires = 0)
    and (board is $board or board is null)
    and (user = $user or
        (type = 1 and user in saa) or
        (type = 2 and user in ancestors)
    );
