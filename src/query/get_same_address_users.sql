select user
from address natural join
    (select address from address where user = $user)
group by user
having user <> $user;
