insert into post (
    post,
    thread,
    board,
    date,
    subject,
    message,
    user,
    email,
    name,
    address
) values (?, ?, ?, unixepoch(), ?, ?, ?, ?, ?, ?);
