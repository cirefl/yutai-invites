insert into ban (
    board,
    user,
    type,
    date,
    expires,
    reason,
    moderator
) values (?, ?, ?, unixepoch(), ?, ?, ?);
