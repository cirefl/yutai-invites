update post set
    removed = unixepoch(),
    reason = $reason,
    moderator = $moderator
where user = $user and
    ($board is null or board = $board);
