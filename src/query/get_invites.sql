select *
from invite
where user = ? and
    expires > unixepoch() and
    used is null
order by expires desc;
