select * from
    (select count(post) as total_posts from post) join
    (select count(name) as unique_posters from user) join
    (select sum(size) as content_size from image)
;
