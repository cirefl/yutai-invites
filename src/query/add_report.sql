insert into report (
    board,
    thread,
    post,
    reason,
    date,
    user,
    global
) values (?, ?, ?, ?, unixepoch(), ?, ?);
