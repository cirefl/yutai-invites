pub const Type = enum(u8) {
    pub const BaseType = u8;
    individual = 0,
    same_address_descendants = 1,
    descendants = 2,
};

ban_id: usize,
board: ?[]const u8,
user: []const u8,
type: Type,
date: usize,
expires: usize,
reason: []const u8,
moderator: []const u8,

pub fn permanent(self: *const @This()) bool {
    return self.expires == 0;
}

pub fn expired(self: *const @This(), date: i64) bool {
    return self.expires <= date and !self.permanent();
}
