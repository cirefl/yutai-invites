const std = @import("std");
const sqlite = @import("sqlite");
const root = @import("root");

const c = root.c;
const data = root.data;
const query = root.query;
const model = root.model;
const util = model.util;

const Context = root.Context;
const Address = root.Address;

const Error = model.Error;

pub fn get(
    context: Context,
    board_opt: ?[]const u8,
    user: []const u8,
) ![]data.Ban {
    return try util.all(data.Ban, context, "get_bans", .{
        .user = user,
        .board = board_opt,
    });
}

pub fn add(
    context: Context,
    board_opt: ?[]const u8,
    user: []const u8,
    @"type": data.Ban.Type,
    length: i64,
    reason: []const u8,
    mod: []const u8,
) !void {
    const expires = if (length > 0) std.time.timestamp() + length else 0;
    try util.exec(
        context,
        "add_ban",
        .{ board_opt, user, @"type", expires, reason, mod },
    );
}

pub const State = enum { all, expired, permanent, temporary };

pub fn pages(
    context: Context,
    board: ?[]const u8,
    comptime state: State,
) usize {
    const b = bounds(state);
    const args = .{ b.min, b.max, board };
    return util.pages(context, "get_bans_count", args);
}

pub fn page(
    context: Context,
    board: ?[]const u8,
    comptime state: State,
    p: usize,
) ![]data.Ban {
    const config = context.config;
    const q = "get_bans_page";
    const b = bounds(state);

    return try util.all(data.Ban, context, q, .{
        b.min,
        b.max,
        board,
        config.page_length,
        p *| config.page_length,
    });
}

pub fn dismiss(
    context: Context,
    user: data.User,
    reason: []const u8,
    board: ?[]const u8,
    id: usize,
) !void {
    const q = "close_ban";

    try util.exec(context, q, .{ reason, user.name, id, board });
}

fn bounds(comptime state: State) util.Bounds {
    return switch (state) {
        .all => .{ .min = 0, .max = std.math.maxInt(i64) },
        .expired => .{ .min = 1, .max = std.time.timestamp() },
        .temporary => .{
            .min = std.time.timestamp(),
            .max = std.math.maxInt(i64),
        },
        .permanent => .{},
    };
}
