const std = @import("std");
const sqlite = @import("sqlite");
const root = @import("root");

const c = root.c;
const data = root.data;
const query = root.query;
const model = root.model;
const util = model.util;

const Context = root.Context;

const DataError = model.DataError;
const Error = model.Error;

pub fn add(context: Context, user: []const u8, addr: std.net.Address) !void {
    if (context.config.log_user_ip) {
        var buf: [64]u8 = undefined;
        const address = try util.bufAddressStr(&buf, addr);
        try util.exec(context, "add_address", .{ user, address });
    }
}

pub fn get(context: Context, user: data.User) ![][]const u8 {
    const q = "get_addresses";
    const name = user.name;
    return try util.all([]const u8, context, q, .{name});
}

pub fn getSame(context: Context, user: data.User) ![][]const u8 {
    const q = "get_same_address_users";
    return try util.all([]const u8, context, q, .{ .user = user.name });
}
