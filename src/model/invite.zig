const std = @import("std");
const sqlite = @import("sqlite");
const root = @import("root");

const c = root.c;
const data = root.data;
const query = root.query;
const model = root.model;
const util = model.util;

const Context = root.Context;

const DataError = model.DataError;

pub fn new(context: Context, user: data.User) !void {
    const config = context.config;

    if (!model.user.isRoot(context, user.name)) {
        if (count(context, user) >= config.max_user_invites) {
            return error.TooManyInvites;
        }
    }

    var buf: [64]u8 = undefined;
    const code = util.randStr(context.rng, &buf);
    const expires = std.time.timestamp() + config.invite_duration;

    try util.exec(context, "add_invite", .{ code, user.name, expires });
}

pub fn get(context: Context, invite: []const u8) !data.Invite {
    const q = "get_invite";
    return (try util.oneAlloc(data.Invite, context, q, .{invite})) orelse
        error.NotFound;
}

pub fn all(context: Context, user: data.User) ![]data.Invite {
    return try util.all(data.Invite, context, "get_invites", .{user.name});
}

pub fn use(context: Context, invite: []const u8) !void {
    try util.exec(context, "delete_invite", .{invite});
}

pub fn count(context: Context, user: data.User) usize {
    const range = std.time.timestamp() - context.config.invite_duration;
    return util.oneSize(context, "get_invites_count", .{ user.name, range });
}
