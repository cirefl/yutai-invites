const std = @import("std");
const sqlite = @import("sqlite");
const root = @import("root");

const c = root.c;
const data = root.data;
const query = root.query;
const model = root.model;
const util = model.util;

const Context = root.Context;

const DataError = model.DataError;

pub const Node = struct {
    name: []const u8,
    list: std.ArrayListUnmanaged(*Node),

    pub fn init(alloc: std.mem.Allocator, name: []const u8) !@This() {
        return .{
            .name = try alloc.dupe(u8, name),
            .list = .{},
        };
    }

    pub fn deinit(self: *@This(), alloc: std.mem.Allocator) void {
        for (self.list.items) |item| {
            item.deinit(alloc);
        }
        self.list.deinit(alloc);
        alloc.free(self.name);
        alloc.destroy(self);
    }
};

pub fn get(context: Context, user: data.User) !*Node {
    const alloc = context.alloc;

    const anc = try getAncestors(context, user);
    defer root.util.free(alloc, anc);

    const des = try getDescendants(context, user);
    defer root.util.free(alloc, des);

    const anc_tree_opt = try ancTree(alloc, anc);
    const des_tree = try desTree(alloc, user.name, des);

    return if (anc_tree_opt) |anc_tree| blk: {
        const tree_root = anc_tree[0];
        const last = anc_tree[1];
        try last.list.append(alloc, des_tree);
        break :blk tree_root;
    } else des_tree;
}

pub fn add(context: Context, parent: []const u8, user: []const u8) !void {
    try util.exec(context, "add_parent", .{ parent, parent, user });
}

fn getAncestors(context: Context, user: data.User) ![]data.Parent {
    return try util.all(data.Parent, context, "get_ancestors", .{user.name});
}

fn getDescendants(context: Context, user: data.User) ![]data.Parent {
    return try util.all(data.Parent, context, "get_descendants", .{user.name});
}

fn ancTree(alloc: std.mem.Allocator, raw: []data.Parent) !?[2]*Node {
    return if (raw.len == 0) null else blk: {
        const root_node = try alloc.create(Node);
        root_node.* = try Node.init(alloc, raw[0].root);
        var last = root_node;
        for (raw[1..]) |row| {
            const node = try alloc.create(Node);
            node.* = try Node.init(alloc, row.root);
            try last.list.append(alloc, node);
            last = node;
        }
        break :blk .{ root_node, last };
    };
}

fn desTree(alloc: std.mem.Allocator, name: []const u8, raw: []data.Parent) !*Node {
    var map = std.StringHashMap(*Node).init(alloc);
    defer map.deinit();

    const root_node = try alloc.create(Node);
    root_node.* = try Node.init(alloc, name);
    try map.put(root_node.name, root_node);

    for (raw) |row| {
        if (map.get(row.parent)) |parent| {
            const node = try alloc.create(Node);
            node.* = try Node.init(alloc, row.child);
            try parent.list.append(alloc, node);
            try map.put(node.name, node);
        }
    }

    return root_node;
}
