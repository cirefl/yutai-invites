const std = @import("std");
const root = @import("root");
const http = @import("apple_pie");

const view = root.view;
const model = root.model;
const handler = root.handler;
const util = handler.util;

const Context = root.Context;

pub fn get(
    context: Context,
    response: *http.Response,
    request: http.Request,
    name: []const u8,
) !void {
    const user_opt = try util.getUserOpt(context, request);
    defer root.util.free(context.alloc, user_opt);

    const tmp = try model.user.get(context, name);
    defer root.util.free(context.alloc, tmp);

    const tree = try model.parent.get(context, tmp);
    defer tree.deinit(context.alloc);

    const addresses = try model.address.get(context, tmp);
    defer root.util.free(context.alloc, addresses);

    const same_address_users = try model.address.getSame(context, tmp);
    defer root.util.free(context.alloc, same_address_users);

    const user_data = try model.user.info(context, user_opt, null);

    try util.render(response, view.global.ban, .{
        .name = name,
        .tree = tree,
        .addresses = addresses,
        .same_address_users = same_address_users,
        .config = context.config,
        .user_data_opt = user_data,
    });
}

pub fn post(
    context: Context,
    response: *http.Response,
    request: http.Request,
    name: []const u8,
) !void {
    const user = try util.getUser(context, request);
    defer root.util.free(context.alloc, user);

    var form = try request.form(context.alloc);
    defer form.deinit(context.alloc);

    const length_str = try util.getField(form, "length");
    const reason = try util.getField(form, "reason");
    const type_str = try util.getField(form, "type");

    const ban_images = form.fields.get("ban_images") != null;
    const images = form.fields.get("images") != null;
    const images_permanent = form.fields.get("images_permanent") != null;
    const posts = form.fields.get("posts") != null;
    const posts_permanent = form.fields.get("posts_permanent") != null;

    const @"type" = try util.parseBanType(type_str);
    const length = try util.parseLength(length_str);

    try model.ban.add(context, null, name, @"type", length, reason, user.name);

    if (ban_images) {
        try model.post_image.banByUser(context, name, user.name);
    } else if (images) {
        if (images_permanent) {
            try model.post_image.deleteByUserPermanent(context, name, user.name);
        } else {
            try model.post_image.deleteByUser(context, null, name, user.name);
        }
    }

    if (posts) {
        if (posts_permanent) {
            try model.post.deleteByUserPermanent(context, name);
        } else {
            try model.post.deleteByUser(context, null, name, user.name, reason);
        }
    }

    try util.message(context, response, "User Banned!", user);
}
