const std = @import("std");
const root = @import("root");
const http = @import("apple_pie");

const view = root.view;
const model = root.model;
const handler = root.handler;
const util = handler.util;

const Context = root.Context;

pub const Data = struct {
    board: []const u8,
    name: []const u8,
};

pub fn get(
    context: Context,
    response: *http.Response,
    request: http.Request,
    args: Data,
) !void {
    const user_opt = try util.getUserOpt(context, request);
    defer root.util.free(context.alloc, user_opt);

    const board = try model.board.one(context, args.board);
    defer root.util.free(context.alloc, board);

    const tmp = try model.user.get(context, args.name);
    defer root.util.free(context.alloc, tmp);

    const tree = try model.parent.get(context, tmp);
    defer tree.deinit(context.alloc);

    const addresses = try model.address.get(context, tmp);
    defer root.util.free(context.alloc, addresses);

    const same_address_users = try model.address.getSame(context, tmp);
    defer root.util.free(context.alloc, same_address_users);

    const user_data = try model.user.info(context, user_opt, args.board);

    try util.render(response, view.board.ban, .{
        .board = board,
        .name = args.name,
        .tree = tree,
        .addresses = addresses,
        .same_address_users = same_address_users,
        .config = context.config,
        .user_data_opt = user_data,
    });
}

pub fn post(
    context: Context,
    response: *http.Response,
    request: http.Request,
    args: Data,
) !void {
    const board = args.board;

    const user = try util.getUser(context, request);
    defer root.util.free(context.alloc, user);

    var form = try request.form(context.alloc);
    defer form.deinit(context.alloc);

    const length_str = try util.getField(form, "length");
    const reason = try util.getField(form, "reason");
    const type_str = try util.getField(form, "type");
    const images = form.fields.get("images") != null;
    const posts = form.fields.get("posts") != null;

    const @"type" = try util.parseBanType(type_str);
    const length = try util.parseLength(length_str);

    try model.ban.add(context, board, args.name, @"type", length, reason, user.name);

    if (images) {
        try model.post_image.deleteByUser(context, board, args.name, user.name);
    }

    if (posts) {
        try model.post.deleteByUser(context, board, args.name, user.name, reason);
    }

    try util.message(context, response, "User Banned!", user);
}
