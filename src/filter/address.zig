const std = @import("std");
const root = @import("root");
const http = @import("apple_pie");

const model = root.model;
const handler = root.handler;
const filter = root.filter;
const util = handler.util;

const Context = root.Context;

const Error = filter.Error;

pub fn add(
    context: Context,
    _: *http.Response,
    request: http.Request,
) !void {
    const user = try handler.util.getUser(context, request);
    defer root.util.free(context.alloc, user);
    try model.address.add(context, user.name, request.address);
}
