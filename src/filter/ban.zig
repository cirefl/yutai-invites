const std = @import("std");
const root = @import("root");
const http = @import("apple_pie");

const model = root.model;
const handler = root.handler;
const filter = root.filter;
const view = root.view;
const util = handler.util;

const Context = root.Context;

const Error = filter.Error;

pub fn global(
    context: Context,
    response: *http.Response,
    request: http.Request,
) !void {
    try any(context, request, response, null);
}

pub fn local(
    context: Context,
    response: *http.Response,
    request: http.Request,
    board: []const u8,
) !void {
    try any(context, request, response, board);
}

fn any(
    context: Context,
    request: http.Request,
    response: *http.Response,
    opt: ?[]const u8,
) !void {
    const user = try util.getUser(context, request);
    defer root.util.free(context.alloc, user);

    const bans = try model.ban.get(context, opt, user.name);
    defer root.util.free(context.alloc, bans);

    if (bans.len == 0) return;

    const user_data = try model.user.info(context, user, null);

    response.status_code = .forbidden;
    try util.render(
        response,
        view.user.banned,
        .{
            .user = user.name,
            .bans = bans,
            .user_data_opt = user_data,
            .config = context.config,
        },
    );

    return Error.BannedAddress;
}
